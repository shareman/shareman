##############################################################################
#  This file is part of shareman.                                            #
#                                                                            #
#  Copyright (C) 2013, 2015  Rouven Spreckels  <n3vu0r@nevux.org>            #
#                                                                            #
#  shareman is free software: you can redistribute it and/or modify          #
#  it under the terms of the GNU Affero General Public License version 3 as  #
#  published by the Free Software Foundation on 19 November 2007.            #
#                                                                            #
#  shareman is distributed in the hope that it will be useful,               #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Affero General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Affero General Public License  #
#  along with shareman.  If not, see <http://www.gnu.org/licenses/>.         #
##############################################################################

_$(PACKAGE_TARNAME)_users() {
	find "$2" -mindepth 1 -maxdepth 1 -uid 0 -exec sh -c \
		'user=$(basename "$1"); groups "$user" | grep -q '$1' && echo "$user"' \
	_ {} \;
}

_$(PACKAGE_TARNAME)()
{
	local cur=${COMP_WORDS[COMP_CWORD]}
	local pr1=${COMP_WORDS[COMP_CWORD-1]}
	local pr2=${COMP_WORDS[COMP_CWORD-2]}
	case $COMP_CWORD in
		1)
			COMPREPLY=( $(compgen -W 'on no -h --help -v --version -a --addresses' -- $cur) )
			if [ ! "${COMPREPLY[0]:0:1}" = '-' ]; then
				COMPREPLY=( $(compgen -W "$(_$(PACKAGE_TARNAME)_users $(sftp_group) $(homedir))" -- $cur) ${COMPREPLY[@]} )
			fi
		;;
		2)
			case "$pr1" in
				no|on|-h|--help|-v|--version|-a|--addresses)
				;;
				*)
					COMPREPLY=( $(compgen -W 'mk mv mr ro rw rm' -- $cur) )
				;;
			esac
		;;
		3)
			case "$pr1" in
				ro|rw|rm)
					compopt -o dirnames
				;;
				mv|mr)
					local IFS=$'\n'
					COMPREPLY=( $( cd $(homedir)/"$pr2" && compgen -o plusdirs -- $cur) )
				;;
			esac
		;;
		4)
			case "$pr2" in
				mv)
					compopt -o dirnames
				;;
			esac
		;;
	esac
}

complete -o filenames -F _$(PACKAGE_TARNAME) $(PACKAGE_TARNAME)
