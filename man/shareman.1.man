.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.\"  This file is part of shareman.                                            "
.\"                                                                            "
.\"  Copyright (C) 2013, 2015  Rouven Spreckels  <n3vu0r@nevux.org>            "
.\"                                                                            "
.\"  shareman is free software: you can redistribute it and/or modify          "
.\"  it under the terms of the GNU Affero General Public License version 3 as  "
.\"  published by the Free Software Foundation on 19 November 2007.            "
.\"                                                                            "
.\"  shareman is distributed in the hope that it will be useful,               "
.\"  but WITHOUT ANY WARRANTY; without even the implied warranty of            "
.\"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             "
.\"  GNU Affero General Public License for more details.                       "
.\"                                                                            "
.\"  You should have received a copy of the GNU Affero General Public License  "
.\"  along with shareman.  If not, see <http://www.gnu.org/licenses/>.         "
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.\"
.TH $(PACKAGE_TARNAME) 1 "March 25, 2015" "$(PACKAGE_TARNAME)-$(PACKAGE_VERSION)" "$(PACKAGE_NAME)"
.SH NAME
$(PACKAGE_TARNAME) - Manage chrooted sftp users and their shares.
.SH SYNOPSIS
.B $(PACKAGE_TARNAME)
\fIUSER\fR [\fIACTION\fR]
.br
.B $(PACKAGE_TARNAME)
\fISWITCH\fR
.br
.B $(PACKAGE_TARNAME)
\fIOPTION\fR
.SH DESCRIPTION
Shell script to manage chrooted sftp \fIUSERS\fR and their \fISHARES\fR simplifying you to share files with others, setup or remove a new \fIUSER\fR for sharing with, setup or remove single \fISHARES\fR, distinguish between in-place and mountable \fISHARES\fR, setup or remove permanent \fISHARES\fR for incoming files, distinguish between read-only and read-write access, enable or disable sharing at all, have all this done securely.
.TP
\fIUSER\fR, \fIACTION\fR, \fISWITCH\fR, \fIOPTION\fR, \fISHARE\fR, and \fIDIRECTORY\fR will be auto-completed by pressing \fBTAB\fR.
.TP
\fISHARES\fR can be accessed by \fBsshfs(1)\fR, \fBsftp(1)\fR, or other \fBsftp\fR compatible clients like \fBlftp(1)\fR.
.TP
\fBUMODE\fR is the file permission mode representing the current file mode creation mask (\fBUMASK\fR) as given by \fBumask(1)\fR.
.SH ACTIONS - User Management
With no \fIACTION\fR, create \fIUSER\fR with primary group \fB$(sftp_group)\fR or modify it if existing. Group \fB$(sftp_group)\fR is created if required and is used to chroot users created via \fB$(PACKAGE_TARNAME)\fR.
.TP
\fBrm\fR
Remove \fIUSER\fR if its home directory is empty of \fISHARES\fR.
.SH ACTIONS - Permanent Share Management
\fISHARE\fR is a permanent directory in the home directory of \fIUSER\fR.
.TP
\fBmk\fR \fISHARE\fR
Create \fISHARE\fR with read and write access.
.TP
\fBmv\fR \fISHARE\fR [\fIDIRECTORY\fR=$HOME/\fISHARE\fR]
Move content of \fISHARE\fR to \fIDIRECTORY\fR and recursively apply owner and group of \fIDIRECTORY\fR to its newly created files and set permissions of them to \fBUMODE\fR. Append suffix .~N~ to already existing files, where N is an incremented number starting by 1. Create \fIDIRECTORY\fR if required.
.TP
\fBmr\fR \fISHARE\fR
Remove \fISHARE\fR if empty. Double\-pressing \fBTAB\fR will list all \fISHARES\fR of this \fIUSER\fR. \fISHARES\fR being mount points of \fIDIRECTORY\fR are only removable via \fBrm\fR \fIDIRECTORY\fR because since they are mounted, they are in use and thus not removable.
.SH ACTIONS - Mountable Share Management
\fISHARE\fR is the mount point and thus the basename of \fIDIRECTORY\fR in the home directory of \fIUSER\fR.
.TP
These \fISHARES\fR need to be unmounted before rebooting being not permanent but don't require content moving. Otherwise those forgotten \fISHARES\fR remotelly occure as empty directories without write access becoming now also removable via \fBmr\fR \fISHARE\fR without the need of specifying an appropriate \fIDIRECTORY\fR whose basename matches this \fISHARE\fR.
.TP
Files inside \fISHARES\fR remotelly created will only be locally accessable after \fBrm\fR \fIDIRECTORY\fR. Local files are only remotely readable or writable if read or write access is granted to the the group of \fIDIRECTORY\fR via \fBchmod g+rw\fR \fIFILES\fR, respectively, where \fIFILES\fR belong to the same group, \fIDIRECTORY\fR does.
.TP
\fBro\fR \fIDIRECTORY\fR
Mount \fIDIRECTORY\fR with read access only even if write access is granted via file mode permissions. Create \fIDIRECTORY\fR if not existing.
.TP
\fBrw\fR \fIDIRECTORY\fR
Mount \fIDIRECTORY\fR with read and write access. Create \fIDIRECTORY\fR if not existing.
.TP
\fBrm\fR \fIDIRECTORY\fR
Unmount \fIDIRECTORY\fR and recursively apply owner and group of \fIDIRECTORY\fR to its files and set permissions of them to \fBUMODE\fR. Permanent \fISHARES\fR are removed via \fBmr\fR \fISHARE\fR.
.SH SWITCHES
.TP
\fBon\fR
Enable sharing by restarting service \fBssh\fR. For the first time, \fBsshd\fR configuration file will be modified in order to chroot users of group \fB$(sftp_group)\fR. A backup file will be created being a marker to prevent further modification tries.
.TP
\fBno\fR
Disable sharing by stopping service \fBssh\fR.
.SH OPTIONS
.TP
\fB\-h\fR, \fB\-\-help\fR
Print usage information.
.TP
\fB\-v\fR, \fB\-\-version\fR
Print version information.
.TP
\fB\-a\fR, \fB\-\-addresses\fR
Print network addresses of active devices. This might be your ethernet and/or your Wi-Fi devices.
.SH EXAMPLES
.TP
.B $(PACKAGE_TARNAME) -a
No superuser privileges are required.
.TP
.B sudo $(PACKAGE_TARNAME) on
Useful to only start service \fBssh\fR if required.
.TP
.B sudo $(PACKAGE_TARNAME) no
Stop service \fBssh\fR if no longer required.
.TP
.B sudo $(PACKAGE_TARNAME) buddy
Create user buddy if not existing. In both cases, ask for a new passphrase of this user.
.TP
.B sudo $(PACKAGE_TARNAME) buddy rm
Remove user buddy.
.TP
.B sudo $(PACKAGE_TARNAME) buddy ro .
Mount current directory with read access only.
.TP
.B sudo $(PACKAGE_TARNAME) buddy rw .
Mount current directory with read and write access.
.TP
.B sudo $(PACKAGE_TARNAME) buddy rm .
Unmount current directory.
.TP
.B sudo $(PACKAGE_TARNAME) buddy mk Incoming
Create permanent directory in the home directory of buddy with read and write access.
.TP
.B sudo $(PACKAGE_TARNAME) buddy mv Incoming .
Move content of permanent directory Incoming in the home directory of buddy to current directory.
.TP
.B sudo $(PACKAGE_TARNAME) buddy mr Incoming .
Remove permanent directory Incoming in the home directory of buddy if empty.
.SH FILES
.TP
.B $(bindir)/$(PACKAGE_TARNAME)
The shell script we are talking about
.TP
.B $(datarootdir)/$(PACKAGE_TARNAME)/doc/man/$(PACKAGE_TARNAME).1
This man page
.TP
.B $(sysconfdir)/bash_completion.d/$(PACKAGE_TARNAME)
Bash completion script
.TP
.B $(sshd_config)
Modified \fBsshd\fR configuration file
.TP
.B $(sshd_config)~$(PACKAGE_TARNAME)
Backup of original \fBsshd\fR configuration file
.TP
.B $(homedir)/\fIUSER\fR
Home directory of \fIUSERS\fR. Directories of \fIUSERS\fR of group \fB$(sftp_group)\fR are owned by \fBroot\fR but belong to the primary group \fIUSER\fR.
.SH COPYRIGHT
Copyright \(co 2011-2015  Rouven Spreckels  <$(PACKAGE_BUGREPORT)>
.PP
Every year in a range, inclusive, is a copyrightable year as if it would
be listed individually.
.PP
$(PACKAGE_NAME) is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation on 19 November 2007.
.PP
$(PACKAGE_NAME) is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
.PP
You should have received a copy of the GNU Affero General Public License
along with $(PACKAGE_NAME).  If not, see <http://www.gnu.org/licenses/>.
.SH WEBSITE
<$(PACKAGE_URL)>
.SH AUTHOR
Rouven Spreckels  <$(PACKAGE_BUGREPORT)>
.SH SEE ALSO
.B umask(1) chmod(1) ssh(1) sshfs(1) sftp(1) lftp(1)
