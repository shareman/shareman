#!/bin/sh

##############################################################################
#  This file is part of shareman.                                            #
#                                                                            #
#  Copyright (C) 2011-2015  Rouven Spreckels  <n3vu0r@nevux.org>             #
#                                                                            #
#  Every year in a range, inclusive, is a copyrightable year as if it would  #
#  be listed individually.                                                   #
#                                                                            #
#  shareman is free software: you can redistribute it and/or modify          #
#  it under the terms of the GNU Affero General Public License version 3 as  #
#  published by the Free Software Foundation on 19 November 2007.            #
#                                                                            #
#  shareman is distributed in the hope that it will be useful,               #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Affero General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Affero General Public License  #
#  along with shareman.  If not, see <http://www.gnu.org/licenses/>.         #
##############################################################################

# Description: Manage chrooted sftp users and their shares.

# Pseudo shell to prevent users of $group to login and maybe bypass their chroot jail.
shell=/bin/false
# Users of this group will be chrooted by sshd.
group=sftp
# Path of home directories
home=/home
# Path of sshd configuration file
sshd=/etc/ssh/sshd_config
# Configuration of sshd matching only users of $group
conf="
Match Group $group
	ChrootDirectory %h
	X11Forwarding no
	AllowTcpForwarding no
	PasswordAuthentication yes
	ForceCommand internal-sftp"

# Documentation
script=$(basename "$0")
help="NAME
  $script - Manage chrooted sftp users and their shares.
SYNOPSIS
  $script USER [ACTION]
  $script SWITCH/OPTION
ACTIONS - User Management:
      With no ACTION, create USER or modify it if existing.
  rm  Remove USER if its home directory is empty of SHARES.
ACTIONS - Permanent Share Management:
  mk SHARE                    Create SHARE with read and write access.
  mv SHARE [DIR=\$HOME/SHARE]  Move content of SHARE to DIR.
  mr SHARE                    Remove SHARE if empty.
ACTIONS - Mountable Share Management:
  ro DIR  Mount DIR with read access only. Create DIR if not existing.
  rw DIR  Mount DIR with read and write access. Create DIR if not existing.
  rm DIR  Unmount DIR and make its files accessable.
SWITCHES:
  on  Enable sharing after modifying sshd to chroot users of group $group.
  no  Disable sharing.
OPTIONS:
  -h, --help       Print usage information.
  -v, --version    Print version information.
  -a, --addresses  Print network addresses of active LAN/Wi-Fi devices."

# Script information
name=shareman
version=1.1.2
contact=n3vu0r@nevux.org
website=http://shareman.nevux.org/
info="$script-$version

Copyright (C) 2011-2015  Rouven Spreckels  <$contact>

Every year in a range, inclusive, is a copyrightable year as if it would
be listed individually.

$name is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation on 19 November 2007.

$name is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with $name.  If not, see <http://www.gnu.org/licenses/>.

Website: <$website>"

# Append . if not present before printing $1.
print () {
	end=
	if [ ! "$(echo "$1" | grep '\.$')" ]; then
		end='.'
	fi
	echo "$1$end"
}
# Print $1 redirected to stderr.
error () {
	print "$1" 1>&2
}
# Print $2 before terminating with $1 as error type.
finish () {
	type=$1
	shift
	number=0
	message=$1
	case $type in
		clean)
		;;
		on)
			message="Enabled $1"
		;;
		no)
			message="Disabled $1"
		;;
		usage)
			number=1
		;;
		root)
			number=2
			message='Superuser privileges are required'
		;;
		busy)
			number=3
			message="Directory '$1' is in use"
		;;
		nouse)
			number=4
			message="Directory '$1' isn't mounted on '$2'"
		;;
		mkdir)
			number=5
			message="Can't create directory '$1'"
		;;
		isdir)
			number=6
			message="Directory '$1' doesn't exist"
		;;
		rmdir)
			number=7
			message="Can't remove directory '$1'"
		;;
		user)
			number=8
			message="User '$1' doesn't exist"
		;;
		group)
			number=9
			message="User '$1' doesn't belong to group '$2'"
		;;
		syntax)
			number=10
		;;
		modify)
			number=11
			message="Configuration file '$1' doesn't exist"
		;;
		control)
			number=12
			message="Can't control $1"
		;;
		host)
			number=13
			message="Can't get network addresses"
		;;
		*)
			number=14
			message="Incorrect type '$type' for message '$1'"
		;;
	esac
	if [ $type = clean ]; then
		if [ "$message" ]; then
			print "$message"
		fi
	else
		print "$message" 1>&2
		if [ $type = usage ]; then
			print "Try \`$script --help' for more information." 1>&2
		fi
	fi
	exit $number
}
# Restore stty if necessary before terminating.
cancel () {
	if [ "$sttyconf" ]; then
		stty $sttyconf
	fi
	if [ "$getecho" = true ]; then
		echo
	fi
	finish clean "Cancelled by user"
}
# Emulate read -s.
sread () {
	sttyconf=$(stty -g)
	stty -echo
	eval "read $1"
	stty $sttyconf
}
# Ask user for $3 and saves input in $2 while hiding or showing ($1=hidden/shown) the input.
get() {
	getecho=true
	printf "$3: "
	if [ $1 = hidden ]; then
		sread $2
		echo
	elif [ $1 = shown ]; then
		eval "read $2"
	fi
	getecho=false
}
# Get user and group of $1.
getug () {
	stat -c %U:%G "$1"
}
# Change owner of $1 to owner of its parent directory.
chpar () {
	chown $(getug "$(dirname "$1")") "$1"
}
# Generate mode representing umask.
umode () {
	# Get the mask.
	mask=$(umask)
	pos=1
	for ugo in u g o; do
		pos=$(($pos+1))
		# Split umask in user, group, and other and calculate the correspondig permissions.
		eval "$ugo=$((7-$(expr substr $mask $pos 1)))"
	done
	# Check for symbolic mode.
	if [ "$1" = s ]; then
		# Generate symbolic permissions.
		for ugo in u g o; do
			eval "
			# Split permissions in read, write, and execute.
			r=\$((\$$ugo/4))
			$ugo=\$((\$$ugo%4))
			w=\$((\$$ugo/2))
			X=\$((\$$ugo%2))
			# Interpret read, write, and execute as symbolic ones and merge them to permissions.
			$ugo=
			for Xwr in X w r; do
				eval \"
				if [ \\$\$Xwr -gt 0 ]; then
					$ugo=\$Xwr\$$ugo
				fi\"
			done"
		done
		# Merge permissions to symbolic mode.
		echo a-x,u=$u,g=$g,o=$o
	else
		# Check for file mode.
		if [ ! "$1" = d ]; then
			# Remove execution permision if present.
			for ugo in u g o; do
				eval "$ugo=\$((\$$ugo-\$$ugo%2))"
			done
		fi
		# Merge permissions to mode.
		echo $u$g$o
	fi
}
# For each file or directory in ${>1} owned by $user apply owner and group of $1 and setup permissions.
chsub () {
	# Stores double quoted arguments ${>1}.
	sub='"$2"'
	for i in $(seq 3 $#); do
		sub="$sub \"\$$i\""
	done
	# Search for files and directories in $sub owned by $user and
	# change owner and group to owner and group of $1 and
	# change permissions to umode.
	eval "find $sub -user $user -exec chown $(getug "$1") {} \; -exec chmod $(umode s) {} \;"
}

# Executes cancel() on <Ctrl+C> keystroke.
trap "cancel" INT

# Expect at least one argument (a user, an option, or a switch).
if [ $# = 0 ]; then
	finish usage 'No user, switch, or option specified'
fi

# Check for 1 argument usage.
if [ $# = 1 ]; then
	# Check first argument for option -h, --help.
	if [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
		finish clean "$help"
	fi
	# Check first argument for option -v, --version.
	if [ "$1" = '-v' ] || [ "$1" = '--version' ]; then
		finish clean "$info"
	fi
	# Check first argument for option -a, --addresses.
	if [ "$1" = '-a' ] || [ "$1" = '--addresses' ]; then
		hostname -I 2> /dev/null
		if [ $? = 0 ]; then
			finish clean
		else
			finish host
		fi
	fi
fi

# Check for superuser.
if [ $(id -u) != 0 ]; then
	finish root
fi

# Check for 1 argument usage.
if [ $# = 1 ]; then
	# Check first argument for switch on.
	if [ "$1" = 'on' ]; then
		if [ ! -e "$sshd~$script" ]; then
			if [ ! -e "$sshd" ] || [ -d "$sshd" ]; then
				finish modify "$sshd"
			fi
			sed -i~$script 's/\([ \t]*Subsystem[ \t]\+sftp[ \t]\+\)[^# \t]\+/\1internal-sftp/I' "$sshd"
			if [ ! "$(sed -n '/[ \t]*Match[ \t]\+Group[ \t]\+sftp/Ip' "$sshd")" ]; then
				echo "$conf" >> "$sshd"
			fi
		fi
		service ssh restart 2> /dev/null 1>&2
		if [ $? = 0 ]; then
			finish on 'sharing'
		else
			finish control 'sharing'
		fi
	fi
	# Check first argument for switch no.
	if [ "$1" = 'no' ]; then
		service ssh stop 2> /dev/null 1>&2
		if [ $? = 0 ]; then
			finish no 'sharing'
		else
			finish control 'sharing'
		fi
	fi
fi

action=$2

# Check for 3 to 4 argument usage.
if [ $# -gt 3 ]; then
	if [ ! "$action" = mv ] || [ $# -gt 4 ]; then
		finish usage 'Too many arguments'
	fi
fi

# Check if $user (first argument) either doesn't exist or belongs to $group.
user=$1
groups=$(id -nG "$user" 2> /dev/null)
if [ $? = 0 ] && [ ! "$(echo $groups | grep -F $group)"  ]; then
	finish group "$user" "$group"
fi

dir=$3
home="$home/$user"
# Check either for user management (no $dir specified) or share management ($dir specified).
if [ ! "$dir" ]; then
# User managemant
	if [ "$action" = rm ]; then
		# If $user exists and its $home is empty, remove $user and its $home.
		if [ -d "$home" ]; then
			if [ ! "$(ls -A "$home")" ]; then
				chown $user:$user "$home"
				userdel -r $user 2> /dev/null
				print "User '$user' removed"
			else
				finish busy "$home"
			fi
		fi
	elif [ "$action" ]; then
		# For user management only action rm or no action (create user) is valid.
		finish usage 'Invalid action or no directory specified'
	else
	# Create $user or change its $passphrase.
		# Repeatedly ask for $passphrase.
		while true; do
			get hidden passphrase Passphrase
			if [ ! "$passphrase" ]; then
				error 'Empty, try again'
				continue;
			fi
			get hidden verification Verification
			if [ "$passphrase" = "$verification" ]; then
				break;
			fi
			error 'Mismatch, try again'
		done
		# If $user doesn't exist, create it and add it to $group (also created if necessary).
		if [ ! -d "$home" ]; then
			groupadd -r $group 2> /dev/null
			useradd -MG $group -s "$shell" "$user" 2> /dev/null
			if [ $? != 0 ]; then
				finish syntax "Invalid user name '$user'"
			fi
			mkdir "$home"
			chgrp $user "$home"
		fi
		# Apply $passphrase to $user.
		echo "$user:$passphrase" | chpasswd
		print "Setup of user '$user' applied"
	fi
else
# Share managemant
	# Check $home to be existing.
	if [ ! -d "$home" ]; then
		finish user "$user"
	fi
	# Resolve $dir.
	dir=$(readlink -m "$dir")
	# A writable directory or mount point of $dir
	share="$home/$(basename "$dir")"
	# Check for non-mounting actions.
	if [ "$action" = mk ]; then
		# Create $share.
		mkdir "$share" 2> /dev/null
		if [ $? != 0 ]; then
			finish mkdir "$share"
		fi
		# Make $share writable for $user through its group.
		chgrp $user "$share"
		chmod g+w "$share"
		finish clean "Directory '$share' created"
	elif [ "$action" = mv ]; then
		dir=$4
		# Check $dir to be specified.
		if [ ! "$dir" ]; then
			dir="$PWD/$(basename "$share")"
		fi
		# Check $share to be existing.
		if [ ! -d "$share" ]; then
			finish isdir "$share"
		fi
		# Check $share to be not empty.
		if [ "$(ls -A "$share")" ]; then
			# Check $dir to be existing.
			if [ ! -d "$dir" ]; then
				# Create $dir.
				mkdir "$dir" 2> /dev/null
				if [ $? != 0 ]; then
					finish mkdir "$dir"
				fi
				# Apply owner from parent directory.
				chpar "$dir"
			fi
			# Recursively apply owner and group of $dir and clean permissions of $share/*.
			chsub "$dir" "$share"/*
			# Move content of $share to $dir.
			mv --backup=numbered "$share"/* "$dir"
			print "Moved content of '$share' to '$dir'"
		fi
		finish clean
	elif [ "$action" = mr ]; then
		# Check $share to be existing.
		if [ -d "$share" ]; then
			# Check $share to be empty.
			if [ "$(ls -A "$share")" ]; then
				finish busy "$share"
			fi
			# Remove $share.
			rmdir "$share" 2> /dev/null
			if [ $? != 0 ]; then
				finish rmdir "$dir"
			fi
			print "Directory '$share' removed"
		fi
		finish clean
	fi
	# Check for mounting actions.
	if [ "$action" = rm ]; then
		# Check $dir to be existing.
		if [ ! -d "$dir" ]; then
			finish isdir "$dir"
		fi
	elif [ "$action" = ro ] || [ "$action" = rw ]; then
		# Check $dir to be existing.
		if [ ! -d "$dir" ]; then
			# Create $dir.
			mkdir "$dir" 2> /dev/null
			if [ $? != 0 ]; then
				finish mkdir "$dir"
			fi
			# Apply owner from parent directory.
			chpar "$dir"
		fi
	else
		finish usage "Invalid action '$action' specified"
	fi
	# If $share (mount point of $dir) and $dir are listed mounted, unmount them.
	# This locks $share against $user to safely make further changes (chown, chmod, ro<>rw).
	mounted=$(findmnt -nr "$share")
	if [ "$mounted" ]; then
		# Check if $share is mount point of $dir.
		if [ "$(echo "$mounted" | grep -F "$dir")" ]; then
			umount "$share" 2> /dev/null
			if [ $? != 0 ]; then
				finish busy "$share"
			fi
		else
			finish nouse "$share" "$dir"
		fi
	fi
	if [ "$action" = rm ]; then
	# If $share exists, remove it.
		if [ -d "$share" ]; then
			# Check $share to be empty.
			if [ "$(ls -A "$share")" ]; then
				finish busy "$share"
			fi
			# Recursively apply owner and group of $dir and clean its permissions.
			chsub "$dir" "$dir"
			# If no more shares to owner of $dir are present, remove $user from group of owner of $dir.
			if [ ! "$(mount | grep -F "$home")" ]; then
				usermod -G $(echo $(id -Gn $user | sed -e "s/$(stat -c %U "$dir")//") | tr ' ' ',') $user
			fi
			rmdir "$share"
			print "Directory '$share' unmounted"
		fi
	elif [ "$action" = rw ] || [ "$action" = ro ]; then
	# Mount with read and write access or remount with read-only access.
		# Create $share.
		mkdir -p "$share"
		# Recursively apply owner and group of $dir and clean its permissions.
		chsub "$dir" "$dir"
		# If mounting with read and write access, make $dir writable for its group.
		if [ "$action" = rw ]; then
			chmod g+w "$dir"
		fi
		# Mount $dir on mount point $share.
		mount -B "$dir" "$share"
		# Remount $dir on mount point $share with read-only access (remount necessary).
		if [ "$action" = ro ]; then
			mount -o remount,ro,bind "$share"
		fi
		# Add $user to group of owner of $dir.
		# This must be done after read-only remount to ensure $user has no write access through
		# manually given group rights even for a very short time in case of read-only remount.
		usermod -aG $(stat -c %U "$dir") $user
		print "Directory '$share' mounted $action on '$dir'"
	fi
fi
